import sys

from setuptools import setup

setup(
    name="flatpak-gitlab-importer",
    version="0.0.1",
    author="Thibault Saunier",
    author_email="tsaunier@igalia.com",
    description=("Import flatpak bundles artifacts into flatpak repository"),
    license="GPL",
    keywords="flatpak, gitlab",
    url="http://packages.python.org/flatpak-gitlab-importer",
    long_description_markdown_filename='README.md',
    classifiers=[
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License (GPL)"
    ],
    install_requires=['xdg', 'flask', 'flask-restful'],
    scripts=['flatpak-gitlab-importer', 'flatpak-gitlab-importer-server'],
)