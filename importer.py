#!/bin/env python3
import argparse
import gitlab
import io
import json
import os
import tempfile
import subprocess
import sys
import zipfile

from xdg import XDG_CACHE_HOME, XDG_CONFIG_HOME

class FlatpakGitLabImporter:
    def __init__(self):
        self.uri = "https://gitlab.gnome.org/"
        self.token = os.environ.get('GITLAB_TOKEN')
        self.product = None
        self.flatpak_repo = None
        self.gl = None
        self.cache = None

    def load_config(self):
        config = os.path.join(XDG_CONFIG_HOME, "flatpak-gitlab-importer", 'config.json')
        print("Reading %s" % config)

        try:
            with open(config) as c:
                configs = json.load(c)
                print(configs)
                if self.uri is None:
                    self.uri = configs['uri']
                if self.token is None:
                    self.token = configs['token']
                if self.product is None:
                    self.product = configs['product']
                if self.flatpak_repo is None:
                    self.flatpak_repo = configs['flatpak-repo']

        except FileNotFoundError:
            pass


    def run(self, job_id=None):
        self.load_config()
        print("URI: %s Product: %s" % (self.uri, self.product))
        self.gl = gitlab.Gitlab(self.uri, private_token=self.token)

        self.project = self.gl.projects.get(self.product)
        jobs = self.project.jobs.list()

        artifacts = None
        for job in jobs:
            if job.id != job_id:
                continue

            artifacts = job.artifacts()
            break
                

        if not artifacts:
            print("Could not find any artifacts.")
            return (False, "Could not find any artifacts.")

        zip_artifacts = zipfile.ZipFile(io.BytesIO(artifacts))

        bundle = None
        for zfile in zip_artifacts.filelist:
            if zfile.filename.endswith('.flatpak'):
                bundle = zip_artifacts.extract(zfile.filename)
                break

        if not bundle:
            print('Could not find a Flatpak bundle in artifact in job %s' % job.id)
            return (False, 'Could not find a Flatpak bundle in artifact in job %s' % job.id)

        print("Importing repo")
        try:
            subprocess.check_output(['flatpak', 'build-import-bundle', self.flatpak_repo, bundle],
                stderr=subprocess.STDOUT)
            return (True, None)
        except Exception as e:
            print("Importing failed:")
            print(str(e.__dict__))
            return (False, str(e.__dict__))